local composer = require( "composer" )
local utils = require("utils")

display.setStatusBar( display.HiddenStatusBar )

math.randomseed( os.time() )

composer.gotoScene( "scenes.menu" )