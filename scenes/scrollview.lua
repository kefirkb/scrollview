
local composer = require( "composer" )
local widget = require("widget")

local scene = composer.newScene()
local scrollView 

local networkListener
local lastPosY = 0
local images = {}
local imageIndex = 0

local function download() 
	local params = {}
	params.progress = true

	for i = 1,#arrayRefs do
		network.download(
    		arrayRefs[i],
    		"GET",
    		networkListener,
    		params,
    		"copy"..i..".png",
    		system.TemporaryDirectory
		) 
	end
end

 
networkListener = function ( event )

    if ( event.isError ) then
    	print("error download image")

    	elseif ( event.phase == "ended" ) then
        	local status, myImage = pcall(display.newImage, event.response.filename, event.response.baseDirectory) 
        	
        	if(myImage) then
                fitImage(myImage,display.viewableContentWidth)
        		images[imageIndex] = myImage
        		myImage.alpha = 0
        		transition.to(myImage, {time=1000,alpha = 1.0 } )

        		myImage.x= display.contentCenterX
        		myImage.y = lastPosY

        		lastPosY = lastPosY + display.viewableContentWidth
        
        		scrollView:insert( myImage )

   			else
    			local errText = display.newText("Error download image", display.contentCenterX, 700, native.systemFont, 44 )
    			images[imageIndex] = errText
    			errText:setFillColor(0, 0, 0 )
    			errText.x= display.contentCenterX
        		errText.y = lastPosY
        		scrollView:insert(errText)
        		lastPosY = lastPosY + display.viewableContentWidth
    		end
    		imageIndex = imageIndex + 1	
    end
end

local function scrollListener( event )

    --[[local phase = event.phase
    if ( phase == "began" ) then print( "Scroll view was touched" )
    elseif ( phase == "moved" ) then print( "Scroll view was moved" )
    elseif ( phase == "ended" ) then print( "Scroll view was released" )
    end

    if ( event.limitReached ) then
        if ( event.direction == "up" ) then print( "Reached bottom limit" )
        elseif ( event.direction == "down" ) then print( "Reached top limit" )
        elseif ( event.direction == "left" ) then print( "Reached right limit" )
        elseif ( event.direction == "right" ) then print( "Reached left limit" )
        end
    end
	]]
    return true
end

function scene:create( event )

	local sceneGroup = self.view
	download()
	
	scrollView = widget.newScrollView( 
		{
			top = 10,
			left = 0,
			width = display.contentWidth,
			height = display.contentHeight,
			scrollWidth = 0.0,
			listener = scrollListener
		}
	 )
    scrollView:setIsLocked( true, "horizontal" )

	lastPosY = display.contentCenterY

end

function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase
	if ( phase == "will" ) then

	elseif ( phase == "did" ) then
	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	for k,v in pairs(images) do
		table.remove(display,images[k])
		table.remove(images, k)
	end
end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )


return scene


