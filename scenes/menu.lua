
local composer = require( "composer" )

local scene = composer.newScene()

status,arrayRefs = pcall(loadRefs)


local function gotoViewScroll()
	composer.gotoScene( "scenes.scrollview" )
end

local function exit()
	os.exit()
end


function scene:create( event )

    local sceneGroup = self.view

    local viewButton = display.newText( sceneGroup, "Scroll View", display.contentCenterX, 700, native.systemFont, 44 )
    viewButton:setFillColor( 0.82, 0.86, 1 )

    local exitButton = display.newText( sceneGroup, "Exit", display.contentCenterX, 810, native.systemFont, 44 )
    exitButton:setFillColor( 0.75, 0.78, 1 )

    viewButton:addEventListener( "tap", gotoViewScroll )
    exitButton:addEventListener( "tap", exit )

end

function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)
	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen
	end
end

function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)
	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

local function onComplete( event )
    if ( event.action == "clicked" ) then
        local i = event.index
        if ( i == 1 ) then
           os.exit()
 		end   
    end
end


--------------handle exceptions------------------------------------------------------- 
if (not status) then
	local alert = native.showAlert( "Error", "Error loadind source images.json", { "OK" }, onComplete )
end

return scene
