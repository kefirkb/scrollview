FILE_PATH = system.pathForFile("assets/images.json")

local json = require("json")

--[[
	get object with image references
]]
function loadRefs() 
	  	local imageRefs =json.decodeFile(FILE_PATH)
	  		
	  	if not imageRefs or not imageRefs['images'] then 
	  		error("Decode failed!")
	  	end
	  	return imageRefs['images']
end

--[[
	scale image
]]
function fitImage( displayObject, fitWidth, enlarge )
	local scaleFactor = fitWidth / displayObject.width 
	displayObject:scale( scaleFactor, scaleFactor )
end

